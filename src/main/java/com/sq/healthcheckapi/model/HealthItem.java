package com.sq.healthcheckapi.model;

import com.sq.healthcheckapi.enums.HealthStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthItem {
    private Long id;
    private LocalDate dateCreate;
    private String name;
    private String healthStatus;
    private Boolean isGoHome;
}
